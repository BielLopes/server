import { Request, Response } from'express'

import db from '../database/connection';
import convertHourToMinutes from '../utils/convertHourToMinutes';

interface ScheduleItem {
    week_day: number;
    from: string;
    to: string;
};

export default class ClassesController{

    async index(request:Request, response:Response){
        const filters = request.query;

        if(!filters.week_day || !filters.subject || !filters.time){
            return response.status(400).json({
                error: 'Missing filterss to search classes'
            });
        }

        const subject = filters.subject as string;
        const week_day = filters.week_day as string;
        const time = filters.time as string;

        console.log(
            subject,
            week_day,
            time,
        );

        const timeInMinutes = convertHourToMinutes(time);

        const classes = await db('classes')
            .whereExists(function() {
                this.select('class_schedule.*')
                .from('class_schedule')
                .whereRaw('`class_schedule`.`class_id` = `classes`.`id`')
                .whereRaw('`class_schedule`.`week_day` =  ??', [Number(week_day)])
                .whereRaw('`class_schedule`.`from` <= ??',[timeInMinutes])
                .whereRaw('`class_schedule`.`to` > ??',[timeInMinutes])
            })
            .where('classes.subject','=', subject)
            .join('users', 'classes.user_id', '=', 'users.id')
            .select(['classes.*', 'users.*']);

        return response.json(classes);
    }

    async create(request:Request, response:Response){
    
        const {
            name,
            avatar, 
            whatsapp,
            bio,
            subject,
            cost,
            schedule
        } = request.body;   
        
        const trx = await db.transaction();
    
        try {
            const insertedUsersIds = await trx('users').insert({
                name,
                avatar,
                whatsapp,
                bio,
            });
        
            const user_id = insertedUsersIds[0];
        
            console.log(subject);
        
            const insertedClassesIds = await trx('classes').insert({
                cost,
                subject,
                user_id,
            });
            
            const class_id = insertedClassesIds[0];
        
            const classSchedule = schedule.map((sheculeItem: ScheduleItem)=> {
                return {
                    class_id,
                    week_day: sheculeItem.week_day,
                    from: convertHourToMinutes(sheculeItem.from),
                    to: convertHourToMinutes(sheculeItem.to),
                };
            });
        
            await trx('class_schedule').insert(classSchedule);
        
            await trx.commit();
        
            return response.status(201).send();
    
        } catch (error) {
    
            await trx.rollback();
            
            console.log(error);

            return response.status(400).json({
                error: 'Unexpected error while creating new class'
            });
        }
    }
    
    async all(request:Request, response:Response){

        const classes = await db('classes')
            .join('users', 'classes.user_id', '=', 'users.id');

        return response.json(classes);
    }

    async findById(request:Request, response:Response){

        const clasId = Number(request.params.id);

        const Clas = await db('classes')
            .where('classes.id','=', clasId)
            .join('users', 'classes.user_id', '=', 'users.id')
            .first();

        console.log(clasId, Clas);

        return response.json(Clas);
    }
}
