import express from 'express';

const app = express();
app.use(express.json());

app.get('/users', (request, response) => {
    
    console.log(request.query); //http://localhost:3333/users?page=3&name=G

    const users = [
        { name: "Diego", age: 25},
        { name: "Biel", age: 21},
    ]
    return response.json(users);
});

app.post('/users', (request, response) => {
    //console.log('Acessou a rota');

    console.log(request.body);

    const users = [
        { name: "Diego", age: 25},
        { name: "Biel", age: 21},
    ]
    return response.json(users);
});

app.delete('/users/:id', (request, response) => {
    //console.log('Acessou a rota');

    console.log(request.params);

    const users = [
        { name: "Diego", age: 25},
        { name: "Biel", age: 21},
    ]
    return response.json(users);
});

app.listen(3333); //localhost:3333